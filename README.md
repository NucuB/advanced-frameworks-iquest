# Advanced Frameworks iQuest

## Hotel Booking System

## Scope

	The application is designed to make booking easier than it is.Users should be able to book any room from any hotel, at any time, in a friendly-user manner.

## Functionalities

* Login/Register(Authorize system)
* CRUD operations
* Reserve any room at choice
* Filters for entities

## Prerequisites

* Java JDK 11
* Maven 3.0 or later
* Spring Tool Suite 4/Eclipse/IntelliJ IDEA
* MySQL ( database's name : hotelbookingsystem; username : root; password : root )


## Topics

* Java 8
* Maven
* MySQL 
* Introduction to Spring Framework
* Hibernate
* JPA
* The IoC container
* Validation, Data Binding and Type Conversion
* Spring Expression Language (SpEL)
* Aspect Oriented Programming with Spring
* Data Access
* Spring Security
* Servlet API
* JUnit, Mockito	
* Git