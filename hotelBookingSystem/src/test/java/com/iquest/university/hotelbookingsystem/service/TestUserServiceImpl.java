package com.iquest.university.hotelbookingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.UserMapper;
import com.iquest.university.mapper.UserMapperImpl;
import com.iquest.university.model.User;
import com.iquest.university.repository.UserRepository;
import com.iquest.university.service.UserService;
import com.iquest.university.service.UserServiceImpl;

@SpringBootTest
public class TestUserServiceImpl {

	@Mock
	private UserRepository userRepository;

	private UserMapper userMapper;
	private UserService userService;

	private User user;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		userMapper = new UserMapperImpl();
		userService = new UserServiceImpl(userRepository, userMapper);
		user = new User();
		user.setLastname("Johnny");
		user.setFirstname("Ken");
		user.setUserId(1);
	}

	@Test
	public void testRetrieveAllUsersByLastnameShouldReturnNonEmptyList() {
		when(userRepository.findAllByLastname("Johnny")).thenReturn(Stream.of(user).collect(Collectors.toList()));
		Collection<UserDTO> usersDTO = userService.retrieveAllUsers("Johnny");
		assertEquals(1, usersDTO.size());
	}

	@Test
	public void testFindUserByUserIdShouldReturnNonNullObject() {
		Optional<User> user = Optional.of(this.user);
		when(userRepository.findByUserId(1)).thenReturn(user);
		var userDTO = userService.findUser(1);
		assertEquals("Johnny", userDTO.getLastname());
	}

	@Test
	public void testDeleteUserShouldReturnDeletedUser() {
		Optional<User> user = Optional.of(this.user);
		when(userRepository.deleteByUserId(1)).thenReturn(user);
		var deletedUserDTO = userService.deleteUser(1);
		assertEquals("Johnny", deletedUserDTO.getLastname());
	}

	@Test
	public void testUpdateUserShouldReturnUpdatedUser() {
		Optional<User> user = Optional.of(this.user);
		User updatedUser = new User();
		updatedUser.setUserId(1);
		updatedUser.setFirstname("John");
		updatedUser.setLastname("Kern");
		var userDTO = userMapper.toDto(updatedUser);
		when(userRepository.findByUserId(1)).thenReturn(user);
		when(userRepository.save(Mockito.any(User.class))).thenReturn(updatedUser);
		var returnedUpdatedUserDTO = userService.saveUser(userDTO);
		assertEquals("John", returnedUpdatedUserDTO.getFirstname());
	}

	@Test
	public void testSaveUserWithNullAsParameterShouldThrowException() {
		when(userRepository.save(null)).thenThrow(new InvalidArgumentException("Please insert a valid user"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			userService.saveUser(null);
		});
	}

	@Test
	public void testDeleteUserWithIdAsZeroValueShouldThrowException() {
		when(userRepository.deleteByUserId(0)).thenThrow(new InvalidArgumentException("Please insert a valid ID"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			userService.deleteUser(0);
		});
	}

}
