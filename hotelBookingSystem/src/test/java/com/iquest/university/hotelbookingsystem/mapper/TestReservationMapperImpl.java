package com.iquest.university.hotelbookingsystem.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.ReservationMapper;
import com.iquest.university.mapper.ReservationMapperImpl;
import com.iquest.university.model.Reservation;

public class TestReservationMapperImpl {

	private ReservationMapper reservationMapper;

	@BeforeEach
	public void setUp() {
		reservationMapper = new ReservationMapperImpl();
	}

	@Test
	public void testReturnedDTOFromMapperShouldBeSameAsEntity() {
		Reservation actualReservation = new Reservation();
		actualReservation.setCheckin(LocalDate.now());
		actualReservation.setCheckout(LocalDate.now().plusWeeks(1));

		ReservationDTO expectedReservationDTO = reservationMapper.toDto(actualReservation);

		assertEquals(LocalDate.now(), expectedReservationDTO.getCheckin());
		assertEquals(LocalDate.now().plusWeeks(1), expectedReservationDTO.getCheckout());
	}

	@Test
	public void testReturnedEntityFromMapperShouldBeSameAsDTO() {
		ReservationDTO actualReservationDTO = new ReservationDTO();
		actualReservationDTO.setCheckin(LocalDate.now());
		//actualReservationDTO.setCheckout(LocalDate.now().plusWeeks(1));
		actualReservationDTO.setCheckout(LocalDate.now().minusWeeks(1));
		Reservation expectedReservation = reservationMapper.toEntity(actualReservationDTO);

		assertEquals(LocalDate.now(), expectedReservation.getCheckin());
//		assertEquals(LocalDate.now().plusWeeks(1), expectedReservation.getCheckout());
		assertEquals(LocalDate.now().minusWeeks(1), expectedReservation.getCheckout());
	}

	@Test
	public void testEntityNullAsParameterShouldThrowException() {
		Reservation nullReservation = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			reservationMapper.toDto(nullReservation);
		});
	}

	@Test
	public void testDTONullAsParameterShouldThrowException() {
		ReservationDTO nullReservationDTO = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			reservationMapper.toEntity(nullReservationDTO);
		});
	}
}
