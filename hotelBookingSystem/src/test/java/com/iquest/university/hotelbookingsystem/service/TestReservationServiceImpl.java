package com.iquest.university.hotelbookingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.ReservationMapper;
import com.iquest.university.mapper.ReservationMapperImpl;
import com.iquest.university.mapper.UserMapper;
import com.iquest.university.mapper.UserMapperImpl;
import com.iquest.university.model.Hotel;
import com.iquest.university.model.Reservation;
import com.iquest.university.model.Room;
import com.iquest.university.model.User;
import com.iquest.university.repository.HotelRepository;
import com.iquest.university.repository.ReservationRepository;
import com.iquest.university.repository.RoomRepository;
import com.iquest.university.repository.UserRepository;
import com.iquest.university.service.ReservationService;
import com.iquest.university.service.ReservationServiceImpl;
import com.iquest.university.utility.Luxury;

@SpringBootTest
public class TestReservationServiceImpl {

	@Mock
	private ReservationRepository reservationRepository;
	@Mock
	private HotelRepository hotelRepository;
	@Mock
	private RoomRepository roomRepository;
	@Mock
	private UserRepository userRepository;

	private ReservationService reservationService;
	private ReservationMapper reservationMapper;

	private Room singleRoom;
	private Room doubleRoom;
	private Hotel hotel;
	private User user;
	private Reservation reservation;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		reservationMapper = new ReservationMapperImpl();
		reservationService = new ReservationServiceImpl(reservationRepository, hotelRepository, roomRepository,
				userRepository, reservationMapper);

		user = new User();
		user.setFirstname("Johnny");
		user.setUserId(1);

		singleRoom = new Room(1, 20, true, true, true, true);
		doubleRoom = new Room(2, 40, true, true, true, true);

		hotel = new Hotel("Newz", "Craiova", Luxury.FIVE_STARS, 6, true);
		hotel.setHotelId(1);
		hotel.setRooms(Stream.of(singleRoom, doubleRoom).collect(Collectors.toList()));

		reservation = new Reservation();
		reservation.setHotel(hotel);
		reservation.setRentedRooms(Stream.of(singleRoom, doubleRoom).collect(Collectors.toList()));
		reservation.setUser(user);
		reservation.setCheckin(LocalDate.now());
		reservation.setCheckout(LocalDate.now().plusWeeks(1));
	}

	@Test
	public void testRetrieveAllReservationsByCheckInAndCheckOutShouldReturnNonEmptyList() {
		when(reservationRepository.findAllByCheckinAndCheckout(reservation.getCheckin(), reservation.getCheckout()))
				.thenReturn(Stream.of(reservation).collect(Collectors.toList()));

		Collection<ReservationDTO> reservationsDTO = reservationService
				.retrieveAllReservations(reservation.getCheckin(), reservation.getCheckout());

		assertEquals(1, reservationsDTO.size());
	}

	@Test
	public void testRetrieveAllReservationsBetweenTimeIntervalShouldReturnNonEmptyList() {
		var secondReservation = new Reservation();
		secondReservation.setHotel(hotel);
		secondReservation.setUser(user);
		secondReservation.setRentedRooms(Stream.of(singleRoom, doubleRoom).collect(Collectors.toList()));
		secondReservation.setCheckin(LocalDate.now());
		secondReservation.setCheckout(LocalDate.now().plusWeeks(2));

		when(reservationRepository.findAllByCheckinBetween(LocalDate.now().minusWeeks(1), LocalDate.now().plusWeeks(3)))
				.thenReturn(Stream.of(reservation, secondReservation).collect(Collectors.toList()));

		Collection<ReservationDTO> reservationsDTO = reservationService
				.retrieveAllReservationsInTimeInterval(LocalDate.now().minusWeeks(1), LocalDate.now().plusWeeks(3));

		assertEquals(2, reservationsDTO.size());
	}

	@Test
	public void testRetrieveAllReservationsByUserShouldReturnNonEmptyList() {
		when(userRepository.findByUserId(1)).thenReturn(Optional.of(user));
		when(reservationRepository.findAllByUserUserId(1))
				.thenReturn(Stream.of(reservation).collect(Collectors.toList()));

		UserMapper userMapper = new UserMapperImpl();
		var userDTO = userMapper.toDto(user);

		Collection<ReservationDTO> reservationsDTO = reservationService.retrieveAllReservations(userDTO.getUserId());

		assertEquals(1, reservationsDTO.size());
	}

	@Test
	public void testFindReservationShouldReturnNonNullObject() {
		Optional<Reservation> reservation = Optional.of(this.reservation);
		when(reservationRepository.findByReservationId(1)).thenReturn(reservation);
		var reservationDTO = reservationService.findReservation(1);
		assertEquals(LocalDate.now(), reservationDTO.getCheckin());
	}

	@Test
	public void testDeleteReservationShouldReturnDeletedReservation() {
		Optional<Reservation> reservation = Optional.of(this.reservation);
		when(reservationRepository.deleteByReservationId(1)).thenReturn(reservation);
		var deletedReservationDTO = reservationService.deleteReservation(1);
		assertEquals(LocalDate.now(), deletedReservationDTO.getCheckin());
	}

	@Test
	public void testSaveReservationShouldReturnSavedReservation() {
		var reservationUpdated = new Reservation();
		reservationUpdated.setCheckin(LocalDate.now().plusWeeks(2));
		reservationUpdated.setCheckout(LocalDate.now().plusWeeks(5));
		Optional<Reservation> reservation = Optional.of(this.reservation);

		var reservationUpdatedDTO = reservationMapper.toDto(reservationUpdated);
		reservationUpdatedDTO.setHotelId(1);
		reservationUpdatedDTO.setUserId(1);
		reservationUpdatedDTO.setRentedRoomsIds(Stream.of(1).collect(Collectors.toList()));

		when(reservationRepository.findByReservationId(1)).thenReturn(reservation);
		when(hotelRepository.findByHotelId(1)).thenReturn(Optional.of(hotel));
		when(userRepository.findByUserId(1)).thenReturn(Optional.of(user));
		when(roomRepository.findByRoomId(1)).thenReturn(Optional.of(singleRoom));
		when(reservationRepository.save(Mockito.any(Reservation.class))).thenReturn(reservationUpdated);

		var returnedUpdatedReservationDTO = reservationService.saveReservation(reservationUpdatedDTO);
		assertEquals(LocalDate.now().plusWeeks(2), returnedUpdatedReservationDTO.getCheckin());
	}

	@Test
	public void testAddReservationWithNullAsParameterShouldThrowException() {
		when(reservationRepository.save(null))
				.thenThrow(new InvalidArgumentException("Please insert a valid reservation"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			reservationService.saveReservation(null);
		});
	}

	@Test
	public void testDeleteHotelWithIdAsZeroValueShouldThrowException() {
		when(reservationRepository.deleteByReservationId(0))
				.thenThrow(new InvalidArgumentException("Please insert a valid ID"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			reservationService.deleteReservation(0);
		});
	}

}
