package com.iquest.university.hotelbookingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.iquest.university.dto.RoomDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.RoomMapper;
import com.iquest.university.mapper.RoomMapperImpl;
import com.iquest.university.model.Hotel;
import com.iquest.university.model.Room;
import com.iquest.university.repository.HotelRepository;
import com.iquest.university.repository.RoomRepository;
import com.iquest.university.service.RoomService;
import com.iquest.university.service.RoomServiceImpl;
import com.iquest.university.utility.Luxury;

@SpringBootTest
public class TestRoomServiceImpl {

	@Mock
	private RoomRepository roomRepository;
	@Mock
	private HotelRepository hotelRepository;

	private RoomMapper roomMapper;
	private RoomService roomService;
	private Room singleRoom;
	private Room doubleRoom;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		roomMapper = new RoomMapperImpl();
		roomService = new RoomServiceImpl(roomRepository, roomMapper, hotelRepository);
		singleRoom = new Room(1, 20, true, true, true, true);
		singleRoom.setRoomId(1);
		doubleRoom = new Room(2, 40, true, true, true, true);
		doubleRoom.setRoomId(2);
	}

	@Test
	public void testRetrieveAllRoomsByCapacityShouldReturnNonEmptyList() {
		when(roomRepository.findAllByCapacity(2)).thenReturn(Stream.of(doubleRoom).collect(Collectors.toList()));
		Collection<RoomDTO> roomsDTO = roomService.retrieveAllRooms(2);
		assertEquals(1, roomsDTO.size());
	}

	@Test
	public void testRetrieveAllRoomsByBusyStatusShouldReturnNonEmptyList() {
		when(roomRepository.findAllByBusy(true))
				.thenReturn(Stream.of(singleRoom, doubleRoom).collect(Collectors.toList()));
		Collection<RoomDTO> roomsDTO = roomService.retrieveAllRooms(true);
		assertEquals(2, roomsDTO.size());
	}

	@Test
	public void testFindRoomShouldReturnNonNullObject() {
		Optional<Room> doubleRoom = Optional.of(this.doubleRoom);
		when(roomRepository.findByRoomId(2)).thenReturn(doubleRoom);
		var roomDTO = roomService.findRoom(2);
		assertEquals(40, roomDTO.getPrice());
		assertEquals(2, roomDTO.getRoomId());
	}

	@Test
	public void testDeleteRoomShouldReturnDeletedRoom() {
		Optional<Room> doubleRoom = Optional.of(this.doubleRoom);
		when(roomRepository.deleteByRoomId(2)).thenReturn(doubleRoom);
		var deletedRoomDTO = roomService.deleteRoom(2);
		assertEquals(40, deletedRoomDTO.getPrice());
		assertEquals(2, deletedRoomDTO.getRoomId());
	}

	@Test
	public void testSaveRoomShouldReturnSavedRoom() {
		Hotel hotel = new Hotel("Newz", "Craiova", Luxury.FIVE_STARS, 6, true);
		hotel.setRooms(Stream.of(singleRoom, doubleRoom).collect(Collectors.toList()));
		var roomDTO = roomMapper.toDto(doubleRoom);
		roomDTO.setHotelId(1);
		when(roomRepository.findByRoomId(2)).thenReturn(Optional.of(singleRoom));
		when(hotelRepository.findByHotelId(1)).thenReturn(Optional.of(hotel));
		when(roomRepository.save(Mockito.any(Room.class))).thenReturn(doubleRoom);
		var updatedRoomDTO = roomService.saveRoom(roomDTO);
		assertEquals(40, updatedRoomDTO.getPrice());
		assertEquals(2, updatedRoomDTO.getRoomId());
	}

	@Test
	public void testSaveRoomWithNullAsParameterShouldThrowException() {
		when(roomRepository.save(null)).thenThrow(new InvalidArgumentException("Please insert a valid room"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			roomService.saveRoom(null);
		});
	}

	@Test
	public void testDeleteRoomWithIdAsZeroValueShouldThrowException() {
		when(roomRepository.deleteByRoomId(0)).thenThrow(new InvalidArgumentException("Please insert a valid ID"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			roomService.deleteRoom(0);
		});
	}

}
