package com.iquest.university.hotelbookingsystem.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.UserMapper;
import com.iquest.university.mapper.UserMapperImpl;
import com.iquest.university.model.User;

public class TestUserMapperImpl {

	private UserMapper userMapper;

	@BeforeEach
	public void setUp() {
		userMapper = new UserMapperImpl();
	}

	@Test
	public void testReturnedDTOFromMapperShouldBeSameAsEntity() {
		User actualUser = new User();
		actualUser.setFirstname("Johnny");
		actualUser.setEmail("test@y.com");
		actualUser.setUserId(1);

		UserDTO expectedUserDTO = userMapper.toDto(actualUser);
		assertEquals(expectedUserDTO.getEmail(), actualUser.getEmail());
		assertEquals(expectedUserDTO.getFirstname(), actualUser.getFirstname());
	}

	@Test
	public void testReturnedEntityFromMapperShouldBeSameAsDTO() {
		UserDTO actualUserDTO = new UserDTO();
		actualUserDTO.setFirstname("Johnny");
		actualUserDTO.setEmail("test@y.com");
		actualUserDTO.setUserId(1);

		User expectedUser = userMapper.toEntity(actualUserDTO);
		assertEquals(expectedUser.getEmail(), actualUserDTO.getEmail());
		assertEquals(expectedUser.getFirstname(), actualUserDTO.getFirstname());
	}

	@Test
	public void testEntityNullAsParameterShouldThrowException() {
		User nullUser = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			userMapper.toDto(nullUser);
		});
	}

	@Test
	public void testDTONullAsParameterShouldThrowException() {
		UserDTO nullUserDTO = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			userMapper.toEntity(nullUserDTO);
		});
	}
}
