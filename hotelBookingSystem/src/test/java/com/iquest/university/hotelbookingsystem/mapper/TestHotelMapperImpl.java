package com.iquest.university.hotelbookingsystem.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.iquest.university.dto.HotelDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.HotelMapper;
import com.iquest.university.mapper.HotelMapperImpl;
import com.iquest.university.model.Hotel;
import com.iquest.university.utility.Luxury;

public class TestHotelMapperImpl {

	private HotelMapper hotelMapper;

	@BeforeEach
	public void setUp() {
		hotelMapper = new HotelMapperImpl();
	}

	@Test
	public void testReturnedDTOFromMapperShouldBeSameAsEntity() {
		Hotel actualHotel = new Hotel("Laguna", "Craiova", Luxury.TWO_STARS, 6, true);
		HotelDTO expectedHotelDTO = hotelMapper.toDto(actualHotel);

		assertEquals("Laguna", expectedHotelDTO.getName());
		assertEquals(Luxury.TWO_STARS, expectedHotelDTO.getLuxury());
	}

	@Test
	public void testReturnedEntityFromMapperShouldBeSameAsDTO() {
		HotelDTO actualHotelDTO = new HotelDTO("Laguna", Luxury.TWO_STARS, 6, true);
		Hotel expectedHotel = hotelMapper.toEntity(actualHotelDTO);

		assertEquals("Laguna", expectedHotel.getName());
		assertEquals(Luxury.TWO_STARS, expectedHotel.getLuxury());
	}

	@Test
	public void testEntityNullAsParameterShouldThrowException() {
		Hotel nullHotel = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			hotelMapper.toDto(nullHotel);
		});
	}

	@Test
	public void testDTONullAsParameterShouldThrowException() {
		HotelDTO nullDTO = null;
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			hotelMapper.toEntity(nullDTO);
		});
	}

}
