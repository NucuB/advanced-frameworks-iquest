package com.iquest.university.hotelbookingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.iquest.university.dto.HotelDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.HotelMapper;
import com.iquest.university.mapper.HotelMapperImpl;
import com.iquest.university.model.Hotel;
import com.iquest.university.model.Room;
import com.iquest.university.repository.HotelRepository;
import com.iquest.university.service.HotelService;
import com.iquest.university.service.HotelServiceImpl;
import com.iquest.university.utility.Luxury;
import com.iquest.university.utility.HotelFilter;

@SpringBootTest
public class TestHotelServiceImpl {

	@Mock
	private HotelRepository hotelRepository;

	private HotelService hotelService;
	private HotelMapper hotelMapper;

	private Room singleRoom;
	private Room doubleRoom;
	private Hotel hotel;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		hotelMapper = new HotelMapperImpl();
		hotelService = new HotelServiceImpl(hotelRepository, hotelMapper);
		singleRoom = new Room(1, 20, true, true, true, true);
		doubleRoom = new Room(2, 40, true, true, true, true);
		hotel = new Hotel("Newz", "Craiova", Luxury.FIVE_STARS, 6, true);
		hotel.setHotelId(1);
		hotel.setRooms(Stream.of(singleRoom, doubleRoom).collect(Collectors.toList()));
	}

	@Test
	public void testRetrieveAllHotelsShouldReturnNonEmptyList() {
		HotelFilter filter = new HotelFilter();
		filter.setHotelName("Newz");
		filter.setTown("Craiova");
		when(hotelRepository.getAllHotelsByFilter(filter)).thenReturn(Stream.of(hotel).collect(Collectors.toList()));
		Collection<HotelDTO> hotelsDTO = hotelService.retrieveAllHotels(filter);
		assertEquals(1, hotelsDTO.size());
	}

	@Test
	public void testDeleteHotelShouldReturnDeletedHotel() {
		Optional<Hotel> hotel = Optional.of(this.hotel);
		when(hotelRepository.deleteByHotelId(this.hotel.getHotelId())).thenReturn(hotel);
		var deletedHotelDTO = hotelService.deleteHotel(this.hotel.getHotelId());
		assertEquals("Newz", deletedHotelDTO.getName());
	}

	@Test
	public void testSaveHotelShouldReturnSavedHotel() {
		var hotelUpdated = new Hotel("Flormang", "Craiova", Luxury.FIVE_STARS, 6, true);
		hotelUpdated.setHotelId(1);
		hotelUpdated.setRooms(Stream.of(singleRoom, doubleRoom).collect(Collectors.toList()));
		Optional<Hotel> hotel = Optional.of(this.hotel);
		var hotelDTO = hotelMapper.toDto(hotelUpdated);
		when(hotelRepository.findByHotelId(1)).thenReturn(hotel);
		when(hotelRepository.save(Mockito.any(Hotel.class))).thenReturn(hotelUpdated);
		var addedHotelDTO = hotelService.saveHotel(hotelDTO);
		assertEquals("Flormang", addedHotelDTO.getName());
	}

	@Test
	public void testDeleteHotelWithIdAsZeroValueShouldThrowException() {
		when(hotelRepository.deleteByHotelId(0)).thenThrow(new InvalidArgumentException("Please insert a valid ID"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			hotelService.deleteHotel(0);
		});
	}

	@Test
	public void testSaveHotelWithNullAsParameterShouldTrowException() {
		when(hotelRepository.save(null)).thenThrow(new InvalidArgumentException("Please insert a valid hotel"));
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			hotelService.saveHotel(null);
		});
	}

}
