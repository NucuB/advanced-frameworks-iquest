package com.iquest.university.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.service.UserService;
import com.iquest.university.utility.UserValidator;

@Controller
public class AuthController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserValidator userValidator;

	@RequestMapping(method = RequestMethod.GET, value = "/reg")
	public String viewRegister(Model model) {
		model.addAttribute("userDTO", new UserDTO());
		return "reg";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/reg")
	public String registration(@ModelAttribute("userDTO") UserDTO userForm, BindingResult bindingResult) {
		userValidator.validate(userForm, bindingResult);
		if (bindingResult.hasErrors()) {
			return "error";
		}
		userService.saveUser(userForm);
		return "redirect:/welcome";
	}

}
