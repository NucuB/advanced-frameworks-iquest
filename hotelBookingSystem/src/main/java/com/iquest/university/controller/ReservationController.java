package com.iquest.university.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.service.ReservationService;

@RestController
@RequestMapping(value = "/users", produces = "application/json")
public class ReservationController {

	@Autowired
	private ReservationService reservationService;

	@RequestMapping(method = RequestMethod.GET, value = "{userId}/reservations")
	public Collection<ReservationDTO> getReservations(@PathVariable(name = "userId") long userId) {
		return reservationService.retrieveAllReservations(userId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "{userId}/reservations", consumes = "application/json")
	public ReservationDTO createReservation(@RequestBody ReservationDTO dto,
			@PathVariable(name = "userId") long userId) {
		return reservationService.saveReservation(dto);
	}

}
