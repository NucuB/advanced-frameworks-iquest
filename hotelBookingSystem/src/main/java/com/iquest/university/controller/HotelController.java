package com.iquest.university.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iquest.university.dto.HotelDTO;
import com.iquest.university.service.HotelService;
import com.iquest.university.utility.HotelFilter;
import com.iquest.university.utility.Luxury;

@RestController
@RequestMapping(value = "/hotels", produces = "application/json")
public class HotelController {

	@Autowired
	private HotelService hotelService;

	@RequestMapping(method = RequestMethod.GET)
	public Collection<HotelDTO> getHotels(@RequestParam(name = "town", required = false) String town,
			@RequestParam(name = "name", required = false) String hotelName,
			@RequestParam(name = "luxury", required = false) Luxury luxury) {
		HotelFilter filter = new HotelFilter();
		filter.setHotelName(hotelName);
		filter.setLuxury(luxury);
		filter.setTown(town);
		return hotelService.retrieveAllHotels(filter);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public HotelDTO createHotel(@RequestBody HotelDTO dto) {
		return hotelService.saveHotel(dto);
	}
}
