package com.iquest.university.dto;

import java.util.Collection;

import com.iquest.university.utility.Luxury;

public class HotelDTO {

	private long hotelId;
	private String town;
	private String name;
	private Luxury luxury;
	private Collection<RoomDTO> roomsDTO;
	private int floors;
	private boolean elevator;

	public HotelDTO(String name, Luxury luxury, int floors, boolean elevator) {
		this.name = name;
		this.luxury = luxury;
		this.floors = floors;
		this.elevator = elevator;
	}

	public HotelDTO() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Luxury getLuxury() {
		return luxury;
	}

	public void setLuxury(Luxury luxury) {
		this.luxury = luxury;
	}

	public Collection<RoomDTO> getRoomsDTO() {
		return roomsDTO;
	}

	public void setRoomsDTO(Collection<RoomDTO> rooms) {
		this.roomsDTO = rooms;
	}

	public int getFloors() {
		return floors;
	}

	public void setFloors(int floors) {
		this.floors = floors;
	}

	public boolean hasElevator() {
		return elevator;
	}

	public void setElevator(boolean elevator) {
		this.elevator = elevator;
	}

	public long getHotelId() {
		return hotelId;
	}

	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

}
