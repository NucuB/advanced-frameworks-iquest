package com.iquest.university.dto;

import java.time.LocalDate;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ReservationDTO {

	private long reservationId;
	private long hotelId;
	private long userId;
	@JsonFormat(pattern = "dd-MM-yyyy")
	private LocalDate checkin;
	@JsonFormat(pattern = "dd-MM-yyyy")
	private LocalDate checkout;
	private UserDTO userDTO;
	private HotelDTO hotelDTO;
	private Collection<RoomDTO> roomsDTO;
	private Collection<Integer> rentedRoomsIds;

	public LocalDate getCheckin() {
		return checkin;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

	public LocalDate getCheckout() {
		return checkout;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public long getReservationId() {
		return reservationId;
	}

	public void setReservationId(long reservationId) {
		this.reservationId = reservationId;
	}

	public HotelDTO getHotelDTO() {
		return hotelDTO;
	}

	public void setHotelDTO(HotelDTO hotelDTO) {
		this.hotelDTO = hotelDTO;
	}

	public Collection<RoomDTO> getRoomsDTO() {
		return roomsDTO;
	}

	public void setRoomsDTO(Collection<RoomDTO> roomsDTO) {
		this.roomsDTO = roomsDTO;
	}

	public long getHotelId() {
		return hotelId;
	}

	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Collection<Integer> getRentedRoomsIds() {
		return rentedRoomsIds;
	}

	public void setRentedRoomsIds(Collection<Integer> rentedRoomsIds) {
		this.rentedRoomsIds = rentedRoomsIds;
	}

}
