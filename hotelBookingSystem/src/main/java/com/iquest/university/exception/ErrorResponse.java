package com.iquest.university.exception;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ErrorResponse {

	@JsonFormat
	private int errorStatus;
	@JsonFormat
	private HttpStatus httpStatus;
	@JsonFormat
	private String error;

	public ErrorResponse(String error, int errorStatus, HttpStatus httpStatus) {
		this.errorStatus = errorStatus;
		this.error = error;
		this.httpStatus = httpStatus;
	}

}
