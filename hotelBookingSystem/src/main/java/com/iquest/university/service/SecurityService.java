package com.iquest.university.service;

public interface SecurityService {
	
	String findLoggedInUsername();

}
