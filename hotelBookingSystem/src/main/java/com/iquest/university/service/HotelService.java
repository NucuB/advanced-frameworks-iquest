package com.iquest.university.service;

import java.util.Collection;

import com.iquest.university.dto.HotelDTO;
import com.iquest.university.utility.HotelFilter;

public interface HotelService {

	Collection<HotelDTO> retrieveAllHotels(HotelFilter filter);

	HotelDTO saveHotel(HotelDTO dto);

	HotelDTO deleteHotel(long hotelId);
}
