package com.iquest.university.service;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.exception.HotelNotFoundException;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.exception.ReservationNotFoundException;
import com.iquest.university.exception.UserNotFoundException;
import com.iquest.university.mapper.ReservationMapper;
import com.iquest.university.model.Hotel;
import com.iquest.university.model.Reservation;
import com.iquest.university.model.Room;
import com.iquest.university.model.User;
import com.iquest.university.repository.HotelRepository;
import com.iquest.university.repository.ReservationRepository;
import com.iquest.university.repository.RoomRepository;
import com.iquest.university.repository.UserRepository;

@Service
public class ReservationServiceImpl implements ReservationService {

	private ReservationRepository reservationRepository;
	private HotelRepository hotelRepository;
	private UserRepository userRepository;
	private RoomRepository roomRepository;
	private ReservationMapper reservationMapper;

	public ReservationServiceImpl(ReservationRepository reservationRepository, HotelRepository hotelRepository,
			RoomRepository roomRepository, UserRepository userRepository, ReservationMapper reservationMapper) {
		this.reservationMapper = reservationMapper;
		this.reservationRepository = reservationRepository;
		this.hotelRepository = hotelRepository;
		this.userRepository = userRepository;
		this.roomRepository = roomRepository;
	}

	@Override
	public ReservationDTO findReservation(long id) {
		if (id == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		return reservationMapper.toDto(
				reservationRepository.findByReservationId(id).orElseThrow(() -> new ReservationNotFoundException()));
	}

	@Override
	public Collection<ReservationDTO> retrieveAllReservations(long userId) {
		if (userId == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		var user = userRepository.findByUserId(userId).orElseThrow(() -> new UserNotFoundException());
		return reservationRepository.findAllByUserUserId(user.getUserId()).stream()
																		  .map(reservationMapper::toDto)
																		  .collect(Collectors.toList());
	}

	@Override
	public Collection<ReservationDTO> retrieveAllReservations(LocalDate checkIn, LocalDate checkOut) {
		if (checkIn == null || checkOut == null) {
			throw new InvalidArgumentException("Please insert valid dates");
		}
		return reservationRepository.findAllByCheckinAndCheckout(checkIn, checkOut).stream()
																				   .map(reservationMapper::toDto)
																				   .collect(Collectors.toList());
	}

	@Override
	public Collection<ReservationDTO> retrieveAllReservationsInTimeInterval(LocalDate startTime, LocalDate endTime) {
		if (startTime == null || endTime == null) {
			throw new InvalidArgumentException("Please insert valid dates");
		}
		return reservationRepository.findAllByCheckinBetween(startTime, endTime).stream()
																				.map(reservationMapper::toDto)
																				.collect(Collectors.toList());
	}

	@Override
	public ReservationDTO deleteReservation(long id) {
		if (id == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		return reservationMapper.toDto(
				reservationRepository.deleteByReservationId(id).orElseThrow(() -> new ReservationNotFoundException()));
	}

	@Override
	public ReservationDTO saveReservation(ReservationDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid reservation");
		}
		if (dto.getCheckout().isBefore(dto.getCheckin())) {
			throw new InvalidArgumentException("Wrong check-out date provided");
		}
		Hotel hotel = hotelRepository.findByHotelId(dto.getHotelId()).orElseThrow(() -> new HotelNotFoundException());
		User user = userRepository.findByUserId(dto.getUserId()).orElseThrow(() -> new UserNotFoundException());
		Reservation reservation = reservationMapper.toEntity(dto);
		reservation.setUser(user);
		reservation.setHotel(hotel);
		reservation.setRentedRooms(getRoomsFromHotelAccordingToIds(dto.getRentedRoomsIds()));
		return reservationMapper.toDto(reservationRepository.save(reservation));
	}

	private Collection<Room> getRoomsFromHotelAccordingToIds(Collection<Integer> rentedRoomsIds) {
		var rentedRooms = rentedRoomsIds.stream()
										.map((roomId) -> roomRepository.findByRoomId(roomId).get())
										.collect(Collectors.toList());
		return rentedRooms;
	}

}
