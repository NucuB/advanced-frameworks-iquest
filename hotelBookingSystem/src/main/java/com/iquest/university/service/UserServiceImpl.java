package com.iquest.university.service;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.exception.UserNotFoundException;
import com.iquest.university.mapper.UserMapper;
import com.iquest.university.model.User;
import com.iquest.university.repository.RoleRepository;
import com.iquest.university.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private UserMapper userMapper;
	private RoleRepository roleRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, UserMapper userMapper) {
		this.userMapper = userMapper;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
	}

	@Override
	public UserDTO findUser(long id) {
		if (id == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		return userMapper.toDto(userRepository.findByUserId(id).orElseThrow(() -> new UserNotFoundException()));
	}

	@Override
	public Collection<UserDTO> retrieveAllUsers(String lastname) {
		if (lastname.isBlank()) {
			throw new InvalidArgumentException("Please insert a valid lastname");
		}
		return userRepository.findAllByLastname(lastname).stream()
														 .map(userMapper::toDto)
														 .collect(Collectors.toList());
	}

	@Override
	public UserDTO deleteUser(long id) {
		if (id == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		return userMapper.toDto(userRepository.deleteByUserId(id).orElseThrow(() -> new UserNotFoundException()));
	}

	@Override
	public UserDTO saveUser(UserDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid user");
		}
		User user = userMapper.toEntity(dto);
		user.setPassword(bCryptPasswordEncoder.encode(dto.getPassword()));
		user.setUsername(dto.getUsername());
		user.setRoles(roleRepository.findAll());
		return userMapper.toDto(userRepository.save(user));
	}

	@Override
	public UserDTO findByUsername(String username) {
		return userMapper.toDto(userRepository.findByUsername(username));
	}

}
