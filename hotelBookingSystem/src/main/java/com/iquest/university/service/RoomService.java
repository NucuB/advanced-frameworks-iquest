package com.iquest.university.service;

import java.util.Collection;

import com.iquest.university.dto.RoomDTO;

public interface RoomService {

	Collection<RoomDTO> retrieveAllRooms(int capacity);
	
	Collection<RoomDTO> retrieveAllRooms(boolean busy);

	RoomDTO findRoom(long id);

	RoomDTO saveRoom(RoomDTO dto);
	
	RoomDTO deleteRoom(long id);
}
