package com.iquest.university.service;

import java.time.LocalDate;
import java.util.Collection;

import com.iquest.university.dto.ReservationDTO;

public interface ReservationService {

	Collection<ReservationDTO> retrieveAllReservations(long userId);

	Collection<ReservationDTO> retrieveAllReservations(LocalDate checkIn, LocalDate checkOut);
	
	Collection<ReservationDTO> retrieveAllReservationsInTimeInterval(LocalDate startTime, LocalDate endTime);
	
	ReservationDTO findReservation(long id);

	ReservationDTO saveReservation(ReservationDTO dto);

	ReservationDTO deleteReservation(long id);
}
