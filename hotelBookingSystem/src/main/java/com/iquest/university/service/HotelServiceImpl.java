package com.iquest.university.service;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.iquest.university.dto.HotelDTO;
import com.iquest.university.exception.HotelNotFoundException;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.mapper.HotelMapper;
import com.iquest.university.repository.HotelRepository;
import com.iquest.university.utility.HotelFilter;

@Service
public class HotelServiceImpl implements HotelService {

	private HotelRepository hotelRepository;
	private HotelMapper hotelMapper;

	public HotelServiceImpl(HotelRepository hotelRepository, HotelMapper hotelMapper) {
		this.hotelRepository = hotelRepository;
		this.hotelMapper = hotelMapper;
	}

	@Override
	public HotelDTO deleteHotel(long hotelId) {
		if (hotelId == 0) {
			throw new InvalidArgumentException("Please insert a valid ID");
		}
		return hotelMapper
				.toDto(hotelRepository.deleteByHotelId(hotelId).orElseThrow(() -> new HotelNotFoundException()));
	}
	
	@Override
	public Collection<HotelDTO> retrieveAllHotels(HotelFilter filter) {
		return hotelRepository.getAllHotelsByFilter(filter).stream()
														   .map(hotelMapper::toDto)
														   .collect(Collectors.toList());
	}


	@Override
	public HotelDTO saveHotel(HotelDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid hotel");
		}
		return hotelMapper.toDto(hotelRepository.save(hotelMapper.toEntity(dto)));
	}

}