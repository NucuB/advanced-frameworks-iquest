package com.iquest.university.utility;

import java.util.HashMap;
import java.util.Map;

public class HotelFilter {

	private Map<String, Object> filterMap;
	
	public HotelFilter() {
		filterMap = new HashMap<>();
	}
	
	public void setTown(String town) {
		filterMap.put("town", town);
	}
	
	public void setHotelName(String hotelName) {
		filterMap.put("hotelName", hotelName);
	}
	
	public void setLuxury(Luxury luxury) {
		filterMap.put("luxury", luxury);
	}
	
	public String getTown() {
		return (String) filterMap.get("town");
	}
	
	public String getHotelName() {
		return (String) filterMap.get("hotelName");
	}
	
	public Luxury getLuxury() {
		return  (Luxury) filterMap.get("luxury");
	}
	

	public Map<String, Object> getFilterMap() {
		return filterMap;
	}
	
	
}
