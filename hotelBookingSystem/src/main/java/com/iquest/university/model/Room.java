package com.iquest.university.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.iquest.university.utility.Currency;

@Entity
@Table(name = "rooms")
public class Room {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long roomId;

	@Column(name = "capacity", nullable = false)
	private int capacity;

	@Column(name = "price", nullable = false)
	private int price;

	@Column(name = "wifi", nullable = false)
	private boolean wifi;

	@Column(name = "tv", nullable = false)
	private boolean tv;

	@Column(name = "fridge", nullable = false)
	private boolean fridge;

	@Column(name = "room_status", nullable = false)
	private boolean busy;

	@Column(name = "Currency", nullable = false)
	@Enumerated(EnumType.STRING)
	private Currency currency;

	@ManyToOne
	@JoinColumn(name = "hotel_id")
	private Hotel hotel;

	@ManyToMany(mappedBy = "rentedRooms")
	private Collection<Reservation> reservations;

	@Version
	@Column(name = "version")
	private Integer version;

	public Room(int capacity, int price, boolean wifi, boolean tv, boolean fridge, boolean busy) {
		this.capacity = capacity;
		this.price = price;
		this.wifi = wifi;
		this.tv = tv;
		this.fridge = fridge;
		this.busy = busy;
	}

	public Room() {

	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public boolean hasWifi() {
		return wifi;
	}

	public void setWifi(boolean wifi) {
		this.wifi = wifi;
	}

	public boolean hasTv() {
		return tv;
	}

	public void setTv(boolean tv) {
		this.tv = tv;
	}

	public boolean hasFridge() {
		return fridge;
	}

	public void setFridge(boolean fridge) {
		this.fridge = fridge;
	}

	public boolean isBusy() {
		return busy;
	}

	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public long getRoomId() {
		return roomId;
	}

	public Integer getVersion() {
		return version;
	}

	public Collection<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Collection<Reservation> reservations) {
		this.reservations = reservations;
	}

	public void setRoomId(long roomId) {
		this.roomId = roomId;
	}

}
