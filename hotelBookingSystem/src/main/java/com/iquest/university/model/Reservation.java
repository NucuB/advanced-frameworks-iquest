package com.iquest.university.model;

import java.time.LocalDate;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "reservations")
public class Reservation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long reservationId;

	@Column(name = "checkin", nullable = false)
	private LocalDate checkin;

	@Column(name = "checkout", nullable = false)
	private LocalDate checkout;

	@Version
	@Column(name = "version")
	private Integer version;

	@ManyToMany
	@JoinTable(name = "Rooms_Reservations", joinColumns = {
			@JoinColumn(name = "reservationId", referencedColumnName = "reservationId") }, inverseJoinColumns = {
					@JoinColumn(name = "roomId", referencedColumnName = "roomId") })
	private Collection<Room> rentedRooms;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "hotel_id")
	private Hotel hotel;

	public LocalDate getCheckin() {
		return checkin;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

	public LocalDate getCheckout() {
		return checkout;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}

	public long getReservationId() {
		return reservationId;
	}

	public Integer getVersion() {
		return version;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<Room> getRentedRooms() {
		return rentedRooms;
	}

	public void setRentedRooms(Collection<Room> rentedRooms) {
		this.rentedRooms = rentedRooms;
	}

	public void setReservationId(long reservationId) {
		this.reservationId = reservationId;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

}
