package com.iquest.university.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.iquest.university.utility.Luxury;

@Entity
@Table(name = "hotels")
public class Hotel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long hotelId;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "town", nullable = false)
	private String town;

	@Column(name = "luxury", nullable = false)
	@Enumerated(EnumType.STRING)
	private Luxury luxury;

	@Column(name = "floors", nullable = false)
	private int floors;

	@Column(name = "elevator", nullable = false)
	private boolean elevator;

	@OneToMany(mappedBy = "hotel")
	private Collection<Room> rooms;

	@OneToMany(mappedBy = "hotel")
	private Collection<Reservation> reservations;

	@Version
	@Column(name = "version")
	private Integer version;

	public Hotel(String name, String town, Luxury luxury, int floors, boolean elevator) {
		this.name = name;
		this.luxury = luxury;
		this.floors = floors;
		this.elevator = elevator;
		this.town = town;
	}

	public Hotel() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Luxury getLuxury() {
		return luxury;
	}

	public void setLuxury(Luxury luxury) {
		this.luxury = luxury;
	}

	public int getFloors() {
		return floors;
	}

	public void setFloors(int floors) {
		this.floors = floors;
	}

	public boolean hasElevator() {
		return elevator;
	}

	public void setElevator(boolean elevator) {
		this.elevator = elevator;
	}

	public Collection<Room> getRooms() {
		return rooms;
	}

	public void setRooms(Collection<Room> rooms) {
		this.rooms = rooms;
	}

	public long getHotelId() {
		return hotelId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public Collection<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Collection<Reservation> reservations) {
		this.reservations = reservations;
	}

}
