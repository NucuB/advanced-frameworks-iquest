package com.iquest.university.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iquest.university.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	Collection<User> findAllByLastname(String lastname);
	
	Optional<User> findByUserId(long id);
	
	Optional<User> findByFirstnameAndLastname(String firstname, String lastname);
	
	Optional<User> deleteByUserId(long id);
	
	User findByUsername(String username);
}
