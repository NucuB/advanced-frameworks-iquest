package com.iquest.university.repository;

import java.util.Collection;

import com.iquest.university.model.Hotel;
import com.iquest.university.utility.HotelFilter;

public interface HotelRepositoryCustom {

	Collection<Hotel> getAllHotelsByFilter(HotelFilter filter);
}
