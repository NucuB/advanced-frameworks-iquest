package com.iquest.university.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iquest.university.model.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

	Set<Role> findAll();
}
