package com.iquest.university.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iquest.university.model.Room;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {

	Optional<Room> findByRoomId(long id);
	
	Optional<Room> deleteByRoomId(long id);
	
	Collection<Room> findAllByCapacity(int capacity);
	
	Collection<Room> findAllByBusy(boolean busy);
}
