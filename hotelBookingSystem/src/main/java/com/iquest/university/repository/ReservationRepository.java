package com.iquest.university.repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iquest.university.model.Reservation;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
	
	Optional<Reservation> findByReservationId(long id);
	
	Optional<Reservation> deleteByReservationId(long id);
	
	Collection<Reservation> findAllByUserUserId(long id);
	
	Collection<Reservation> findAllByCheckinAndCheckout(LocalDate checkIn, LocalDate checkOut);
	
	Collection<Reservation> findAllByCheckinBetween(LocalDate startDate, LocalDate endDate);

}
