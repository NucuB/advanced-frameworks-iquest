package com.iquest.university.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iquest.university.model.Hotel;

@Repository
public interface HotelRepository extends CrudRepository<Hotel, Long>, HotelRepositoryCustom{

	Optional<Hotel> findByHotelId(long id);
	
	Optional<Hotel> deleteByHotelId(long id);
	
	
}
