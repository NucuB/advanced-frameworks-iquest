package com.iquest.university.mapper;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.model.User;

public interface UserMapper {

	UserDTO toDto(User entity);

	User toEntity(UserDTO dto);

}
