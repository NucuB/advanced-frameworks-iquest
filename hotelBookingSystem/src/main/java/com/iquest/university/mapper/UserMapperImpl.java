package com.iquest.university.mapper;

import org.springframework.stereotype.Component;

import com.iquest.university.dto.UserDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.model.User;

@Component
public class UserMapperImpl implements UserMapper {

	@Override
	public UserDTO toDto(User entity) {
		if (entity == null) {
			throw new InvalidArgumentException("Please insert a valid user");
		}
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail(entity.getEmail());
		userDTO.setFirstname(entity.getFirstname());
		userDTO.setLastname(entity.getLastname());
		userDTO.setUserId(entity.getUserId());
		return userDTO;
	}

	@Override
	public User toEntity(UserDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid user");
		}
		User user = new User();
		user.setEmail(dto.getEmail());
		user.setFirstname(dto.getFirstname());
		user.setLastname(dto.getLastname());
		user.setUserId(dto.getUserId());
		return user;
	}

}
