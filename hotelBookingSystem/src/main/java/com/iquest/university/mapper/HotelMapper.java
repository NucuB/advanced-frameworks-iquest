package com.iquest.university.mapper;

import com.iquest.university.dto.HotelDTO;
import com.iquest.university.model.Hotel;

public interface HotelMapper {

	HotelDTO toDto(Hotel entity);

	Hotel toEntity(HotelDTO dto);

}
