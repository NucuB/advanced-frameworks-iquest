package com.iquest.university.mapper;

import org.springframework.stereotype.Component;

import com.iquest.university.dto.HotelDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.model.Hotel;

@Component
public class HotelMapperImpl implements HotelMapper {

	@Override
	public HotelDTO toDto(Hotel entity) {
		if (entity == null) {
			throw new InvalidArgumentException("Please insert a valid hotel");
		}
		HotelDTO hotelDTO = new HotelDTO();
		hotelDTO.setHotelId(entity.getHotelId());
		hotelDTO.setName(entity.getName());
		hotelDTO.setTown(entity.getTown());
		hotelDTO.setElevator(entity.hasElevator());
		hotelDTO.setFloors(entity.getFloors());
		hotelDTO.setLuxury(entity.getLuxury());
		return hotelDTO;
	}

	@Override
	public Hotel toEntity(HotelDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid hotel");
		}
		Hotel hotel = new Hotel();
		hotel.setHotelId(dto.getHotelId());
		hotel.setName(dto.getName());
		hotel.setTown(dto.getTown());
		hotel.setElevator(dto.hasElevator());
		hotel.setFloors(dto.getFloors());
		hotel.setLuxury(dto.getLuxury());
		return hotel;
	}

}
