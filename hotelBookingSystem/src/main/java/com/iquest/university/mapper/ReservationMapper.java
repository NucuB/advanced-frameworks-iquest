package com.iquest.university.mapper;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.model.Reservation;

public interface ReservationMapper {

	ReservationDTO toDto(Reservation entity);

	Reservation toEntity(ReservationDTO dto);

}
