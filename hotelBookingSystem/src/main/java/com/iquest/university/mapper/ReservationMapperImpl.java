package com.iquest.university.mapper;

import org.springframework.stereotype.Component;

import com.iquest.university.dto.ReservationDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.model.Reservation;

@Component
public class ReservationMapperImpl implements ReservationMapper {

	@Override
	public ReservationDTO toDto(Reservation entity) {
		if (entity == null) {
			throw new InvalidArgumentException("Please insert a valid reservation");
		}
		ReservationDTO reservationDTO = new ReservationDTO();
		reservationDTO.setCheckin(entity.getCheckin());
		reservationDTO.setCheckout(entity.getCheckout());
		reservationDTO.setReservationId(entity.getReservationId());
		return reservationDTO;
	}

	@Override
	public Reservation toEntity(ReservationDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid reservation");
		}
		Reservation reservation = new Reservation();
		reservation.setCheckin(dto.getCheckin());
		reservation.setCheckout(dto.getCheckout());
		reservation.setReservationId(dto.getReservationId());
		return reservation;
	}
}
