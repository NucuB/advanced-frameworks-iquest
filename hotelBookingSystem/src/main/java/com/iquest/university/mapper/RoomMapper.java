package com.iquest.university.mapper;

import com.iquest.university.dto.RoomDTO;
import com.iquest.university.model.Room;

public interface RoomMapper {

	RoomDTO toDto(Room entity);

	Room toEntity(RoomDTO dto);

}
