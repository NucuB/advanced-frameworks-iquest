package com.iquest.university.mapper;

import org.springframework.stereotype.Component;

import com.iquest.university.dto.RoomDTO;
import com.iquest.university.exception.InvalidArgumentException;
import com.iquest.university.model.Room;

@Component
public class RoomMapperImpl implements RoomMapper {


	@Override
	public RoomDTO toDto(Room entity) {
		if (entity == null) {
			throw new InvalidArgumentException("Please insert a valid room");
		}
		RoomDTO roomDTO = new RoomDTO();
		roomDTO.setBusy(entity.isBusy());
		roomDTO.setCapacity(entity.getCapacity());
		roomDTO.setCurrency(entity.getCurrency());
		roomDTO.setPrice(entity.getPrice());
		roomDTO.setRoomId(entity.getRoomId());
		roomDTO.setFridge(entity.hasFridge());
		roomDTO.setWifi(entity.hasWifi());
		roomDTO.setTv(entity.hasTv());
		return roomDTO;
	}

	@Override
	public Room toEntity(RoomDTO dto) {
		if (dto == null) {
			throw new InvalidArgumentException("Please insert a valid room");
		}
		Room room = new Room();
		room.setBusy(dto.isBusy());
		room.setCapacity(dto.getCapacity());
		room.setCurrency(dto.getCurrency());
		room.setPrice(dto.getPrice());
		room.setRoomId(dto.getRoomId());
		room.setFridge(dto.hasFridge());
		room.setWifi(dto.hasWifi());
		room.setTv(dto.hasTv());
		return room;
	}
}
